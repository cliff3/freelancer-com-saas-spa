# freelancer-com-saas-spa

## Project setup
```
npm install -g @aws-amplify/cli
npm install
```

## AWS setup
```
amplify configure
```
will ask you to sign into the AWS Console

```
amplify configure
```
Create a user with AdministratorAccess to your account to provision AWS resources for you like AppSync, Cognito etc.
Once the user is created, Amplify CLI will ask you to provide the accessKeyId and the secretAccessKey to connect Amplify CLI with your newly created IAM user.

```
amplify init
Do you want to use an existing environment? Yes
Choose the environment you would like to use: dev
Choose your default editor: Visual Studio Code
Select the authentication method you want to use: AWS profile
Please choose the profile you want to use (Use arrow keys): default
```
